﻿using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System.Net;
using TestNinja.Mocking;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class InstallerHelperTests
    {
        [Test]
        public void DownloadInstaller_WhenCalledDownloadComplete_ShouldReturnTrue()
        {
            var downloader = new Mock<IFileDownloader>();
            var helper = new InstallerHelper(downloader.Object);

            var result = helper.DownloadInstaller("abc", "abc");

            Assert.That(result, Is.True);
        }

        [Test]
        public void DownloadInstaller_WhenCalledWithDownloadFailed_ShouldReturnFalse()
        {
            var downloader = new Mock<IFileDownloader>();
            downloader.Setup(m => m.DownloadFile(It.IsAny<string>(),It.IsAny<string>())).Throws<WebException>();
            var helper = new InstallerHelper(downloader.Object);

            var result = helper.DownloadInstaller("customer", "installer");

            Assert.That(result, Is.False);
        }
    }
}
