﻿using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using TestNinja.Mocking;

namespace TestNinja.UnitTests.Mocking
{
    [TestFixture]
    public class VideoServiceTests
    {
        [SetUp]
        public void Setup()
        {
            _filderReader = new Mock<IFileReader>();
            _repository = new Mock<IVideoRepository>();
            _videoService = new VideoService(_filderReader.Object, _repository.Object);
        }

        private Mock<IFileReader> _filderReader;
        private VideoService _videoService;
        private Mock<IVideoRepository> _repository;

        [Test]
        public void GetUnprocessedVideosAsCsv_AFewUnProcessedVideo_ReturnAStringWithIdsOfUnprocessedVideos()
        {
            _repository.Setup(r => r.GetUnProcessedVideos()).Returns(new List<Video>
            {
                new Video{Id = 1},
                new Video{Id = 2},
                new Video{Id = 3}}
            );

            var result = _videoService.GetUnprocessedVideosAsCsv();

            Assert.That(result, Is.EqualTo("1,2,3"));
        }

        [Test]
        public void GetUnprocessedVideosAsCsv_AllVideosAreProcessed_ReturnEmptyString()
        {
            _repository.Setup(r => r.GetUnProcessedVideos()).Returns(new List<Video>());

            var result = _videoService.GetUnprocessedVideosAsCsv();

            Assert.That(result, Is.Empty);
        }

        [Test]
        public void ReadVideoTitle_EmptyFile_ReturnError()
        {
            // Arrange
            _filderReader.Setup(m => m.Read("video.txt")).Returns("");

            // Act
            var result = _videoService.ReadVideoTitle();

            // Assert
            Assert.That(result, Does.Contain("error").IgnoreCase);
        }
    }
}