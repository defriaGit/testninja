﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TestNinja.Mocking;

namespace TestNinja.UnitTests.Mocking
{
    [TestFixture]
    public class HousekeeperServiceTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IStatementProcessor> _statementProcessor;
        private Mock<INotificationService> _notificationService;
        private static readonly DateTime _statementDate = new DateTime(2018, 01, 01);
        private Housekeeper _housekeeper;
        private string _stateFileName = "FileName";

        [SetUp]
        public void SetupUp()
        {
            _housekeeper = new Housekeeper
            {
                Email = "abc",
                FullName = "abc",
                Oid = 1,
                StatementEmailBody = "abc"
            };

            _unitOfWork = new Mock<IUnitOfWork>();
            _unitOfWork.Setup(m => m.Query<Housekeeper>()).Returns(new List<Housekeeper>()
            {
                _housekeeper,
            }.AsQueryable());

            _statementProcessor = new Mock<IStatementProcessor>();
            _statementProcessor.Setup(m => m.SaveStatement(_housekeeper.Oid, _housekeeper.FullName, _statementDate))
                .Returns(_stateFileName);

            _notificationService = new Mock<INotificationService>();
        }


        [Test]
        public void SendStatementEmails_WhenCalledQueryHasAnEmptyListOfHouseKeepers_ReturnsFalse()
        {
            _unitOfWork.Setup(uof => uof.Query<Housekeeper>()).Returns(new List<Housekeeper>().AsQueryable);

            // Act 
            var result = HousekeeperService.SendStatementEmails(_statementDate, _unitOfWork.Object,
                _statementProcessor.Object, _notificationService.Object);

            // Assert
            Assert.That(result, Is.False);
        }


        [Test]
        public void SendStatementEmails_WhenCalledEmptyHousekeeperEmail_SaveStavementIsNotCalled()
        {
            _housekeeper.Email = null;

            // Act 
            var result = HousekeeperService.SendStatementEmails(_statementDate, _unitOfWork.Object,
                _statementProcessor.Object, _notificationService.Object);

            // Assert
            _statementProcessor.Verify(m => m.SaveStatement(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>()),
                Times.Never);
        }

        [Test]
        public void SendStatementEmails_WhenCalledHousekeeperEmail_SaveStavementIsCalled()
        {
            // Act 
            var result = HousekeeperService.SendStatementEmails(_statementDate, _unitOfWork.Object,
                _statementProcessor.Object, _notificationService.Object);

            // Assert
            _statementProcessor.Verify(m => m.SaveStatement(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>()),
                Times.AtLeast(1));
        }

        [Test]
        public void SendStatementEmails_WhenCalledWithStatementFileNameIsNullOrWhiteSpace_EmailFileIsNotCalled()
        {
            // Arrange
            _statementProcessor.Setup(m => m.SaveStatement(_housekeeper.Oid, _housekeeper.FullName, _statementDate))
                .Returns(() => null);


            // Act 
            var result = HousekeeperService.SendStatementEmails(_statementDate, _unitOfWork.Object,
                _statementProcessor.Object, _notificationService.Object);

            // Assert
            _notificationService.Verify(
                m => m.EmailFile(It.IsAny<string>(), 
                    It.IsAny<string>(), 
                    It.IsAny<string>(), 
                    It.IsAny<string>()),
                Times.Never);
        }

        [Test]
        public void SendStatementEmails_WhenCalledWithStatementFileName_EmailFileIsCalled()
        {
            // Act 
            var result = HousekeeperService.SendStatementEmails(_statementDate, _unitOfWork.Object,
                _statementProcessor.Object, _notificationService.Object);

            // Assert
            _notificationService.Verify(
                m => m.EmailFile(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.AtLeast(1));
        }

        [Test]
        public void SendStatementEmails_WhenCalledThrowsAnExpection_ReturnFalse()
        {
            _notificationService.Setup(m => m.EmailFile(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .Throws<Exception>();

            // Act 
            var result = HousekeeperService.SendStatementEmails(_statementDate, _unitOfWork.Object,
                _statementProcessor.Object, _notificationService.Object);

            // Assert
            Assert.That(result, Is.False);
        }
    }
}