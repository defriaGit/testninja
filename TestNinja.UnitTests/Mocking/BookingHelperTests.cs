﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TestNinja.Mocking;

namespace TestNinja.UnitTests.Mocking
{
    [TestFixture]
    public class BookingHelperTests
    {
        private Booking _existingBooking;
        private Mock<IBookingRepository> _repository;

        [SetUp]
        public void Setup()
        {
            // Arrange
            _repository = new Mock<IBookingRepository>();

            _existingBooking = new Booking
            {
                Id = 2,
                ArrivalDate = ArriveOne(2017, 1, 15),
                DepartureDate = DepartOne(2017, 1, 20),
                Reference = "a",
            };

            _repository.Setup(m => m.GetActiveBookings(1)).Returns(new List<Booking>(){_existingBooking}.AsQueryable());
        }

        [Test]
        public void OverlappingBookingsExist_BookingStartsAndfinishesAnExistingBooking_ReturnEmptyString()
        {
            // Act
            var booking = new Booking
            {
                Id = 1,
                ArrivalDate = Before(_existingBooking.ArrivalDate, days: 2 ),
                DepartureDate = Before(_existingBooking.ArrivalDate),
                Reference = "a",
            };
            var result = BookingHelper.OverlappingBookingsExist(booking, _repository.Object);

            // Assert
            Assert.That(result, Is.Empty);

        }

        [Test]
        public void OverlappingBookingsExist_BookingStartsBeforeAndFinishesInTheMiddleOfAnExistingBooking_ReturnExistingBookingReference()
        {
            // Act
            var booking = new Booking
            {
                Id = 1,
                ArrivalDate = Before(_existingBooking.ArrivalDate),
                DepartureDate = After(_existingBooking.ArrivalDate),
                Reference = "a",
            };
            var result = BookingHelper.OverlappingBookingsExist(booking, _repository.Object);

            // Assert
            Assert.That(result, Is.EqualTo(_existingBooking.Reference));

        }

        [Test]
        public void OverlappingBookingsExist_BookingStartsBeforeAndFinishesAfterAnExistingBooking_ReturnExistingBookingReference()
        {
            // Act
            var booking = new Booking
            {
                Id = 1,
                ArrivalDate = Before(_existingBooking.ArrivalDate),
                DepartureDate = After(_existingBooking.DepartureDate),
                Reference = "a",
            };
            var result = BookingHelper.OverlappingBookingsExist(booking, _repository.Object);

            // Assert
            Assert.That(result, Is.EqualTo(_existingBooking.Reference));

        }

        [Test]
        public void OverlappingBookingsExist_BookingStartsAndFinishesInTheMiddleOfAnExistingBooking_ReturnExistingBookingReference()
        {
            // Act
            var booking = new Booking
            {
                Id = 1,
                ArrivalDate = After(_existingBooking.ArrivalDate),
                DepartureDate = Before(_existingBooking.DepartureDate), 
                Reference = "a",
            };
            var result = BookingHelper.OverlappingBookingsExist(booking, _repository.Object);

            // Assert
            Assert.That(result, Is.EqualTo(_existingBooking.Reference));

        }

        [Test]
        public void OverlappingBookingsExist_BookingStartsIntheMiddleOfAnExistingBookingButFinishesAfter_ReturnExistingBookingReference()
        {
            // Act
            var booking = new Booking
            {
                Id = 1,
                ArrivalDate = After(_existingBooking.ArrivalDate),
                DepartureDate = After(_existingBooking.DepartureDate), 
                Reference = "a",
            };
            var result = BookingHelper.OverlappingBookingsExist(booking, _repository.Object);

            // Assert
            Assert.That(result, Is.EqualTo(_existingBooking.Reference));

        }

        [Test]
        public void OverlappingBookingsExist_BookingStartsAndFinishesAfterAnExistingBooking_ReturnEmptyString()
        {
            // Act
            var booking = new Booking
            {
                Id = 1,
                ArrivalDate = After(_existingBooking.DepartureDate),
                DepartureDate = After(_existingBooking.DepartureDate,days: 2 ), 
                Reference = "a",
            };
            var result = BookingHelper.OverlappingBookingsExist(booking, _repository.Object);

            // Assert
            Assert.That(result, Is.Empty);

        }


        [Test]
        public void OverlappingBookingsExist_BookingsOverlapButNewBookingIsCanncelled_ReturnEmptyString()
        {
            // Act
            var booking = new Booking
            {
                Id = 1,
                ArrivalDate = Before(_existingBooking.ArrivalDate),
                DepartureDate = After(_existingBooking.DepartureDate),
                Status = "Cancelled",
                Reference = "a",
            };
            var result = BookingHelper.OverlappingBookingsExist(booking, _repository.Object);

            // Assert
            Assert.That(result, Is.Empty);

        }

        private DateTime Before(DateTime dateTime, int days = 1)
        {
            return dateTime.AddDays(-days);
        }

        private DateTime After(DateTime dateTime, int days = 1)
        {
            return dateTime.AddDays(days);
        }

        private DateTime ArriveOne(int year, int month, int day)
        {
            return new DateTime(year,month, day, 14, 0, 0);
        }

        private DateTime DepartOne(int year, int month, int day)
        {
            return new DateTime(year,month, day, 10, 0, 0);
        }
 
    }
}