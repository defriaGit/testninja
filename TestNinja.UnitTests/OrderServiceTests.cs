﻿using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using TestNinja.Mocking;

namespace TestNinja.UnitTests
{
    [TestFixture()]
    public class OrderServiceTests
    {
        [Test]
        public void PlaceOrder_WhenCalled_StoreTheOrder()
        {
            // Arrange
            var storage = new Mock<IStorage>();
            var orderService = new OrderService(storage.Object);

            // Act
            var order = new Order();
            orderService.PlaceOrder(order);

            // Assert
            storage.Verify(m => m.Store(order));

        }
    }
}
