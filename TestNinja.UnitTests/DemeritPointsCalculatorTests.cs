﻿using NUnit.Framework;
using System;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class DemeritPointsCalculatorTests
    {
        [Test]
        [TestCase(0, 0)]
        [TestCase(64, 0)]
        [TestCase(65, 0)]
        [TestCase(66, 0)]
        [TestCase(70, 1)]
        [TestCase(75, 2)]
    
        public void CalculateDemeritsPoints_WhenCalledWithSpeed_ShouldReturnAccumulatedDemerits(int speed, int expectedResult)
        {
            var dpct = new DemeritPointsCalculator();

            var result = dpct.CalculateDemeritPoints(speed);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        [TestCase(-1)]
        [TestCase(301)]
        public void CalculateDemeritsPoints_WhenCalledWithSpeedOutOfRange_ThrowsArgumentOutOfRangeException(int speed)
        {
            var dpct = new DemeritPointsCalculator();


            Assert.That(() => dpct.CalculateDemeritPoints(speed), Throws.TypeOf<ArgumentOutOfRangeException>());
        }
    }
}